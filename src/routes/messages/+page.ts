import { fetchResource } from "$lib/js/api/fetch";

export async function load() {
  const response = await fetchResource(`messages`, {
    method: "GET",
    headers: {
      "sol-url": "{solUrl}",
      "Authorization": "{authHeader}"
    }
  })

  const json = await response.json()

  return {
    messages: json["messages"],
  }
}
