import { createBubbleError } from "$lib/js/api/errors/bubble";
import { fetchResource } from "$lib/js/api/fetch"
import { routeLoading } from "$lib/stores/route";

export async function getAttachment(messageId: string, attachmentId: string, attachmentName: string) {
  routeLoading.set(true);

  const response = await fetchResource(`messageAttachment/${attachmentId}?message=${messageId}`, {
    method: "GET",
    headers: {
      "sol-url": "{solUrl}",
      "Authorization": "{authHeader}"
    }
  }, {
    errorCallback: () => {
      routeLoading.set(false);
    }
  })

  routeLoading.set(false);

  if (!response.ok) {
    if (response.status === 404) {
      createBubbleError(new Error("Příloha nebyla nalezna."))
    } else {
      createBubbleError(new Error("Nepodařilo se stáhnout přílohu."))
    }
    return;
  }

  const blob = await response.blob();
  const url = window.URL || window.webkitURL;
  const link = url.createObjectURL(blob);

  const a = document.createElement("a");
  a.setAttribute("download", attachmentName);
  a.setAttribute("href", link);
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
}
