import { writable } from "svelte/store";

export const showSettingsBackArrow = writable(false);
