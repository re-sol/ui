import { writable } from "svelte/store";

export const empty = writable(false);
