import { userClass, username } from "$lib/js/auth/store";

export function loadUser() {
  let fullName = localStorage.getItem("fullName")!
  let userTypeText = localStorage.getItem("userTypeText")!


  username.set(fullName);
  userClass.set(userTypeText);
}
