import { fetchResource } from "../fetch";

import { convertDateToString } from "$lib/js/core/date";

export async function getHomework() {
  const res = await fetchResource(`homework?studentId={studentId}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "Authorization": `{authHeader}`,
      "sol-url": `{solUrl}`
    }
  });
  const data = await res.json();

  const cellObject = [];

  for (const homeworkId in Object.keys(data["homeworks"])) {
    // console.log(data["messages"][message])
    const homework = data["homeworks"][homeworkId]

    cellObject.push({
      "date": convertDateToString(new Date(homework["dateEnd"])),
      "name": homework["teacher"]["displayName"],
      "description": homework["name"],
      "link": `/homework/${homework["id"]}`,
      "read": homework["read"]
    })
  }

  // console.log(cellObject)
  return cellObject;
}
