import { fetchResource } from "../fetch";

import { convertDateToString } from "$lib/js/core/date";

const getMonday = (d: Date) => {
  d = new Date(d);
  const day = d.getDay(),
    diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
  return new Date(d.setDate(diff));
}

const getFriday = (d: Date) => {
  // Get the current date
  const today = new Date(d);

  // Calculate the day of the week (0=Sunday, 1=Monday, ..., 6=Saturday)
  const currentDayOfWeek = today.getDay();

  // Calculate the number of days until the next Friday (Friday is 5)
  let daysUntilFriday = 5 - currentDayOfWeek;

  // If today is Friday (currentDayOfWeek is 5), add 7 days to get the next Friday
  if (daysUntilFriday <= 0) {
    daysUntilFriday += 7;
  }

  // Calculate the date of the next Friday
  const nextFriday = new Date(today);
  nextFriday.setDate(today.getDate() + daysUntilFriday);
}


function getGoodDays(specifiedDate?: Date) {
  // old codebase, should be rewritten
  const date = getMonday(specifiedDate || new Date())

  let month: any = date.getMonth() + 1

  const monday = date.getFullYear() + "-" + month + "-" + date.getDate()

  date.setDate(date.getDate() + 4)

  month = date.getMonth() + 1

  const friday = date.getFullYear() + "-" + month + "-" + date.getDate()

  return [monday, friday]
}

export async function getSchedule(specifiedDate?: Date) {
  const [monday, friday] = getGoodDays(specifiedDate ?? undefined)

  const params = new URLSearchParams({
    "studentId": localStorage.getItem("studentId")!,
    "dateFrom": monday,
    "dateTo": friday,
    "semesterId": localStorage.getItem("semesterId")!
  })

  const res = await fetchResource(`timetable?${params}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "Authorization": `{authHeader}`,
      "sol-url": `{solUrl}`
    }
  });

  const data = await res.json();

  // console.log(data) 

  return data
}
