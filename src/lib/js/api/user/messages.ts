import { fetchResource } from "../fetch";

import { convertDateToString } from "$lib/js/core/date";

export async function getMessages() {
  const res = await fetchResource(`messages`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "Authorization": `{authHeader}`,
      "sol-url": `{solUrl}`
    }
  });
  const data = await res.json();

  const cellObject = [];

  for (const message in Object.keys(data["messages"])) {
    // console.log(data["messages"][message])

    cellObject.push({
      "date": convertDateToString(new Date(data["messages"][message]["sentDate"])),
      "name": data["messages"][message]["sender"]["name"],
      "description": data["messages"][message]["title"],
      "link": `/message/${data["messages"][message]["id"]}`,
      "read": data["messages"][message]["read"]
    })
  }

  // console.log(cellObject)
  return cellObject;
}
