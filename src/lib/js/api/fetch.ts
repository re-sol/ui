import { logout } from "../auth/logout";
import { createBubbleError } from "./errors/bubble";
import { getAPIUrlFromStorage } from "./getUrl";

type FetchResourceOptions = {
  studentId?: boolean;
  errorCallback?: (error: Error) => any;
}

export async function fetchResource(url: string, options: any, fetchResourceOptions?: FetchResourceOptions) {
  const apiUrl = await getAPIUrlFromStorage();

  url = url.replace("{studentId}", localStorage.getItem("studentId")!)

  const iterate = (obj: any) => {
    for (let option in obj) {
      if (typeof obj[option] === "object") option = iterate(obj[option])

      if (obj[option] === "{studentId}" && localStorage.getItem("studentId")) {
        obj[option] = localStorage.getItem("studentId")
      } else if (obj[option] === "{studentId}" && !localStorage.getItem("studentId")) {
        createBubbleError(new Error("Tried to get studentId from localStorage, but it was not set."))
      }

      if (obj[option] === "{solUrl}" && localStorage.getItem("solUrl")) {
        obj[option] = localStorage.getItem("solUrl")
      } else if (obj[option] === "{solUrl}" && !localStorage.getItem("solUrl")) {
        createBubbleError(new Error("Tried to get solUrl from localStorage, but it was not set."))
      }

      if (obj[option] === "{authHeader}" && localStorage.getItem("authHeader")) {
        obj[option] = localStorage.getItem("authHeader")
      } else if (obj[option] === "{authHeader}" && !localStorage.getItem("authHeader")) {
        createBubbleError(new Error("Tried to get authHeader from localStorage, but it was not set."))
      }
      // console.log(`${option}: ${obj[option]}`)
    }

    return obj
  }

  const fetchOptions = iterate(options)

  // console.log(fetchOptions)

  const response = await fetch(`${apiUrl}/${url}`, fetchOptions);

  // console.log(fetchResourceOptions.errorCallback)
  if (!response.ok && (fetchResourceOptions == null || fetchResourceOptions.errorCallback == null)) {
    if (response.status === 401) {
      logout();
    }

    createBubbleError(new Error(`Failed to fetch ${apiUrl}/${url}: ${response.status} ${response.statusText}`));
    throw new Error(`Failed to fetch ${apiUrl}/${url}: ${response.status} ${response.statusText}`);
  }

  if (!response.ok && fetchResourceOptions.errorCallback) {
    fetchResourceOptions.errorCallback(new Error(`Failed to fetch ${apiUrl}/${url}: ${response.status} ${response.statusText}`));
  }

  return response;
}
