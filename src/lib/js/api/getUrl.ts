import { createBubbleError } from "./errors/bubble";

export async function getAPIUrl(): Promise<string> {
  console.log("getAPIUrl: fetching")
  const response = await fetch("/api/v1/apiUrl", {
    method: "GET",
    headers: {
      "Content-Type": "application/json"
    }
  });
  const json = await response.json();

  if (json == null || json.apiUrl == null) createBubbleError(new Error("Failed to fetch apiUrl: response was null."));

  return await json.apiUrl;
}

export async function getAPIUrlFromStorage(): Promise<string | undefined> {
  if (localStorage.getItem("apiUrl") == null) {
    const apiUrl = await getAPIUrl();

    localStorage.setItem("apiUrl", apiUrl);
    return apiUrl!;
  } else {
    return localStorage.getItem("apiUrl")!;
  }
}
