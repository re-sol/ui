import { get } from "svelte/store";
import { errors } from "./store";

export const createBubbleError = (error: Error) => {
  console.log(get(errors))
  errors.set([...get(errors), error]);
}
