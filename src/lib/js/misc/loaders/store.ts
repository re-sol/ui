import { writable } from "svelte/store";

export const messagesLoading = writable(false);
export const gradesLoading = writable(false);
export const homeworkLoading = writable(false);
export const eventsLoading = writable(false);
export const behaviorLoading = writable(false);
