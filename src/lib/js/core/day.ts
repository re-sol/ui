export const getDayName = (day: number): string => {
  switch (day) {
    case 0:
      return "Pondělí";
    case 1:
      return "Úterý";
    case 2:
      return "Středa";
    case 3:
      return "Čtvrtek";
    case 4:
      return "Pátek";
    default:
      return "Neznámý den";
  }
}
