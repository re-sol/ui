export function loadTheme() {
  const theme = localStorage.getItem("theme");

  if (theme != null) {
    document.documentElement.classList.add(`theme-${theme}`);
    return;
  } else {
    document.documentElement.classList.add("theme-light");
  }
}

export function setTheme(theme: string) {
  const html = document.documentElement;

  html.classList.forEach((token) => {
    html.classList.remove(token);
  });

  html.classList.add(`theme-${theme}`);

  localStorage.setItem("theme", theme);
}
