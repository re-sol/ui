export const convertDateToString = (date: Date): string => {
  return `${date.getDate()}. ${date.getMonth() + 1}. ${date.getFullYear()}`
}

export function convertDateToCZ(date: Date) {
  const parsed_date = new Date(date)
  const options: any = { day: "numeric", month: "numeric" };

  return parsed_date.toLocaleDateString("cs-cz", options)
}
