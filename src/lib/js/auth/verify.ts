import { getAPIUrlFromStorage } from "../api/getUrl";

export async function verifyAuth(username: string, password: string, solUrl: string) {
  const apiUrl = await getAPIUrlFromStorage();

  const response = await fetch(`${apiUrl}/verify`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Basic ${btoa(`${username}:${password}`)}`,
      "sol-url": solUrl,
    }
  });
  const json = await response.json();
  console.log(json)
  return json;
}
