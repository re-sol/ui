import { createThemes } from "tw-colors";

/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {},
  },
  future: {
    hoverOnlyWhenSupported: true,
  },
  plugins: [
    createThemes({
      light: {
        // Website Colors
        primary: "#5144a2",
        secondary: "#6659b8",
        background: '#ffffff',
        altbackground: "#15191F",
        supplement: "#f2f2f2",
        text: '#15191F',
        description: '#5d6e89',
        alttext: "#F1F1F1",
        substitute: "#5144a2",

        // Grades
        grade1: "#40a02b",
        grade2: "#1e66f5",
        grade3: "#df8e1d",
        grade4: "#fe640b",
        grade5: "#d20f39",

        // More Generic Colors
        // danger: "#d20f39",
        // success: "#40a02b",
        // warning: "#df8e1d",
        // info: "#1e66f5"
        danger: "#ed8796",
        success: "#a6da95",
        warning: "#eed49f",
        info: "#8aadf4"
      },
      dark: {
        // Website Colors
        primary: "#5144a2",
        secondary: "#6659b8",
        background: '#15191F',
        altbackground: "#15191F",
        supplement: "hsl(216, 19%, 18%)",
        text: '#FFF',
        description: '#b3b3b3',
        alttext: "#fff",
        substitute: "#5144a2",

        // Grades
        grade1: "#a6da95",
        grade2: "#8aadf4",
        grade3: "#eed49f",
        grade4: "#f5a97f",
        grade5: "#ed8796",

        // More Generic Colors
        danger: "#ed8796",
        success: "#a6da95",
        warning: "#eed49f",
        info: "#8aadf4"
      }
    })
  ],
  safelist: [
    'bg-primary',
    'bg-secondary',
    'bg-background',
    'bg-grade1',
    'bg-grade2',
    'bg-grade3',
    'bg-grade4',
    'bg-grade5',
    'text-grade1',
    'text-grade2',
    'text-grade3',
    'text-grade4',
    'text-grade5',
  ],
}

